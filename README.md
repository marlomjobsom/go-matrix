# Matrix Code Challenge

A web service with the ability to perform the following operations over a given matrix:
* `/echo`
* `/invert`
* `/flatten`
* `/sum`
* `/multiply`

## Table of contents

[[_TOC_]]


## Build and run

```shell
go build -o out/go-matrix
./out/go-matrix
```

## Usage

### `Echo`

```shell
curl -F 'file=@./matrix.csv' "localhost:8080/echo"
```

### `Invert`
```shell
curl -F 'file=@./matrix.csv' "localhost:8080/invert"
```

### `Flatten`
```shell
curl -F 'file=@./matrix.csv' "localhost:8080/flatten"
```

### `Sum`
```shell
curl -F 'file=@./matrix.csv' "localhost:8080/sum"
```

### `Multiply`
```shell
curl -F 'file=@./matrix.csv' "localhost:8080/multiply"
```

## Documentation

### `go doc`
```shell
go doc -u -all
go doc -u -all ./internal/
go doc -u -all ./internal/controllers/
go doc -u -all ./internal/errs
go doc -u -all ./internal/server
go doc -u -all ./test
```

### `godoc`
```shell
godoc

# Open in your browser
http://localhost:6060/pkg/go-matrix/?m=all
```

## Test

```shell
go test ./test -v
```

## TODO

Improve matrix manipulation algorithm to handle huge matrix inputs with lower computing costs