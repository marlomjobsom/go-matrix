package main

import (
	"go-matrix/internal"
)

// Web service entry-point
func main() {
	internal.Start("8080")
}
