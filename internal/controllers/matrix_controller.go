package controllers

import (
	"encoding/csv"
	"fmt"
	"go-matrix/internal/errs"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
)

// MatrixController returns a new MatrixController instance
func NewMatrixController() *MatrixController {
	return &MatrixController{}
}

// MatrixController holds the REST API end-points
type MatrixController struct{}

// Echo echoes a matrix from the given .csv file
func (ctrl *MatrixController) Echo(w http.ResponseWriter, r *http.Request) {
	matrix, err := ctrl.getQuadraticMatrix(r)

	if err == nil {
		body := ctrl.matrixToString(matrix)
		_, _ = fmt.Fprint(w, body)
	} else {
		http.Error(w, fmt.Sprint(err), 500)
	}
}

// Invert inverts a matrix from the given .csv file
func (ctrl *MatrixController) Invert(w http.ResponseWriter, r *http.Request) {
	matrix, err := ctrl.getQuadraticMatrix(r)

	if err == nil {
		invertedMatrix := ctrl.invert(matrix)
		body := ctrl.matrixToString(invertedMatrix)
		_, _ = fmt.Fprint(w, body)
	} else {
		http.Error(w, fmt.Sprint(err), 500)
	}
}

// Flatten converts a matrix from the given .csv file into an array
func (ctrl *MatrixController) Flatten(w http.ResponseWriter, r *http.Request) {
	matrix, err := ctrl.getQuadraticMatrix(r)

	if err == nil {
		array := ctrl.flatten(matrix)
		body := strings.Join(array, ",")
		_, _ = fmt.Fprint(w, body)
	} else {
		http.Error(w, fmt.Sprint(err), 500)
	}
}

// Sum sums all the elements of a matrix from the given .csv file
func (ctrl *MatrixController) Sum(w http.ResponseWriter, r *http.Request) {
	var total int
	matrix, err := ctrl.getQuadraticMatrix(r)

	if err == nil {
		if total, err = ctrl.sum(matrix); err == nil {
			body := fmt.Sprintf("%d", total)
			_, _ = fmt.Fprint(w, body)
		}
	}

	if err != nil {
		http.Error(w, fmt.Sprint(err), 500)
	}
}

// Multiply multiplies all the elements of a matrix from the given .csv file
func (ctrl *MatrixController) Multiply(w http.ResponseWriter, r *http.Request) {
	var total int
	matrix, err := ctrl.getQuadraticMatrix(r)

	if err == nil {
		if total, err = ctrl.multiply(matrix); err == nil {
			body := fmt.Sprintf("%d", total)
			_, _ = fmt.Fprint(w, body)
		}
	}

	if err != nil {
		http.Error(w, fmt.Sprint(err), 500)
	}
}

// getQuadraticMatrix gets a quadratic matrix from the `file` in the request
func (ctrl *MatrixController) getQuadraticMatrix(r *http.Request) ([][]string, error) {
	var matrix [][]string
	var err error
	var file multipart.File

	if file, err = ctrl.retrieveFile(r); err == nil {
		if matrix, err = ctrl.readFile(file); err == nil {
			err = ctrl.isQuadraticMatrix(matrix)
		}
	}

	return matrix, err
}

// retrieveFile retrieves the `file` from the request
func (ctrl *MatrixController) retrieveFile(r *http.Request) (multipart.File, error) {
	key := "file"
	file, _, err := r.FormFile(key)

	if err != nil {
		err = errs.NewRetrieveFileError(key, err)
	}

	return file, err
}

// readFile reads the `file` from the request
func (ctrl *MatrixController) readFile(file multipart.File) ([][]string, error) {
	defer func () { _ = file.Close() }()
	records, err := csv.NewReader(file).ReadAll()

	if err != nil {
		err = errs.NewReadFileError(err)
	} else if len(records) == 0 {
		err = errs.NewEmptyFileError()
	}

	return records, err
}

// isQuadraticMatrix checks if the given data is a quadratic matrix
func (ctrl *MatrixController) isQuadraticMatrix(data [][]string) error {
	var err error
	rowLength := len(data)

	for _, row := range data {
		if len(row) != rowLength {
			err = errs.NewNonQuadraticMatrixError(rowLength, len(row), data)
		}
	}

	return err
}

// invert inverts a matrix
func (ctrl *MatrixController) invert(matrix [][]string) [][]string {
	var invertedMatrix [][]string

	for x, row := range matrix {
		newRow := make([]string, 0)

		for y := range row {
			newRow = append(newRow, matrix[y][x])
		}

		invertedMatrix = append(invertedMatrix, newRow)
	}

	return invertedMatrix
}

// flatten converts a matrix into an array
func (ctrl *MatrixController) flatten(matrix [][]string) []string {
	array := make([]string, 0)

	for _, row := range matrix {
		for _, cell := range row {
			array = append(array, cell)
		}
	}

	return array
}

// sum sums all the elements of a matrix
func (ctrl *MatrixController) sum(matrix [][]string) (int, error) {
	var value int
	var err error
	total := 0

	outfor:
	for _, row := range matrix {
		for _, cell := range row {
			value, err = ctrl.stringToInt(cell)

			if err != nil {
				break outfor
			}

			total += value
		}
	}

	return total, err
}

// multiply multiples all the elements of a matrix
func (ctrl *MatrixController) multiply(matrix [][]string) (int, error) {
	var value int
	var err error
	total := 1

	outer:
	for _, row := range matrix {
		for _, cell := range row {
			value, err = ctrl.stringToInt(cell)

			if err != nil {
				break outer
			}

			total *= value
		}
	}

	return total, err
}

// matrixToString converts a given matrix into a string
func (ctrl *MatrixController) matrixToString(matrix [][]string) string {
	var joinedMatrix string

	for _, row := range matrix {
		joinedMatrix = fmt.Sprintf("%s%s\n", joinedMatrix, strings.Join(row, ","))
	}

	return strings.TrimSpace(joinedMatrix)
}

// stringToInt converts a given string value into int
func (ctrl *MatrixController) stringToInt(value string) (int, error) {
	castValue, err := strconv.ParseInt(value, 10, 32)

	if err != nil {
		err = errs.NewCastingStringToIntError(err)
	}

	return int(castValue), err
}
