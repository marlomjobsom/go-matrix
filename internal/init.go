package internal

import (
	"go-matrix/internal/controllers"
	"go-matrix/internal/server"
)

// Start setups and runs the HTTP server
func Start(port string) {
	httpServer := server.NewHTTPServer()
	bindMatrixController(httpServer)
	httpServer.Start(port)
}

// bindMatrixController binds the end-point addresses to the MatrixController request handle APIs
func bindMatrixController(server *server.HTTPServer) {
	matrixController := controllers.NewMatrixController()
	server.BindAPI("/echo", matrixController.Echo)
	server.BindAPI("/invert", matrixController.Invert)
	server.BindAPI("/flatten", matrixController.Flatten)
	server.BindAPI("/sum", matrixController.Sum)
	server.BindAPI("/multiply", matrixController.Multiply)
}
