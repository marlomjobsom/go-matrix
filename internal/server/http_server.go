package server

import (
	"fmt"
	"log"
	"net/http"
)

// NewHTTPServer returns a new HTTPServer instance
func NewHTTPServer() *HTTPServer {
	return &HTTPServer{}
}

// HTTPServer is a basic HTTP server
type HTTPServer struct{}

// BindAPI binds an end-point address to a given request handle function
func (server *HTTPServer) BindAPI(address string, ctrlFunc func(writer http.ResponseWriter, request *http.Request)) {
	handleFunc := func(writer http.ResponseWriter, request *http.Request) {
		server.logRequest(request)
		defer server.errorRecover(writer)
		ctrlFunc(writer, request)
	}
	http.HandleFunc(address, handleFunc)
}

// Start starts the HTTP server
func (server *HTTPServer) Start(port string) {
	if len(port) > 0 && port[0] != ':' {
		port = ":" + port
	}

	log.Println("Starting HTTP server at port", port)
	err := http.ListenAndServe(port, nil)
	log.Println(err)
}

// errorRecover recovers from any PANIC raised
func (server *HTTPServer) errorRecover(w http.ResponseWriter) {
	msg := recover()
	if msg != nil {
		http.Error(w, fmt.Sprint(msg), 500)
	}
}

// logRequest logs the incoming request
func (server *HTTPServer) logRequest(request *http.Request) {
	client := fmt.Sprint(request.UserAgent(), "@", request.RemoteAddr)
	dest := fmt.Sprint(request.Host, request.URL)
	log.Println(client, request.Proto, request.Method, dest)
}
