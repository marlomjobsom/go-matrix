package errs

import "fmt"

// NewEmptyFileError returns a new EmptyFileError instance
func NewEmptyFileError() error {
	return &EmptyFileError{}
}

// EmptyFileError indicates an error when receives an empty file
type EmptyFileError struct {}

func (err *EmptyFileError) Error() string {
	return fmt.Sprintf("The given file is empty")
}