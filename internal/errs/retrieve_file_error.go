package errs

import "fmt"

// NewRetrieveFileError returns a new RetrieveFileError instance
func NewRetrieveFileError(key string, reason error) error {
	return &RetrieveFileError{key, reason}
}

// RetrieveFileError indicates an error when trying to retrieve a file
type RetrieveFileError struct {
	key string
	reason error
}

func (err *RetrieveFileError) Error() string {
	template := "Error when retrieving the file sent under the keyword '%s': '%s'"
	return fmt.Sprintf(template, err.key, err.reason)
}