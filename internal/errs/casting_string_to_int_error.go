package errs

import (
	"fmt"
	"strings"
)

// NewCastingStringToIntError returns a new CastingStringToIntError instance
func NewCastingStringToIntError(reason error) error {
	return &CastingStringToIntError{reason}
}

// CastingStringToIntError indicates an error when trying to cast a string to int
type CastingStringToIntError struct {
	reason error
}

func (err *CastingStringToIntError) Error() string {
	msg := err.reason.Error()
	startTarget := "parsing \""
	endTarget := "\": invalid"
	start := strings.Index(msg, startTarget)
	end := strings.Index(msg, endTarget)
	root := msg[start+len(startTarget) : end]

	template := "Error when casting string to int: '%s'"
	return fmt.Sprintf(template, root)
}