package errs

import "fmt"

// NewReadFileError returns a new ReadFileError instance
func NewReadFileError(reason error) error {
	return &ReadFileError{reason}
}

// ReadFileError indicates an error when trying read a file
type ReadFileError struct {
	reason error
}

func (err *ReadFileError) Error() string {
	template := "Error when reading the file: '%s'"
	return fmt.Sprintf(template, err.reason)
}