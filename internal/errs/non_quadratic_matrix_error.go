package errs

import "fmt"

// NewNonQuadraticMatrixError returns a new NonQuadraticMatrixError instance
func NewNonQuadraticMatrixError(rowLength int, colLength int, matrix [][]string) error {
	return &NonQuadraticMatrixError{rowLength, colLength, matrix}
}

// NonQuadraticMatrixError indicates an error when receives a non-quadratic matrix
type NonQuadraticMatrixError struct {
	rowLength int
	colLength int
	matrix [][]string
}

func (err *NonQuadraticMatrixError) Error() string {
	template := "The matrix given is not a quadratic matrix. Found dimension %dx%d. %s"
	return fmt.Sprintf(template, err.rowLength, err.colLength, err.matrix)
}
