package test

import (
	"go-matrix/internal"
	"io/ioutil"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	go internal.Start(PORT)
	TimeSleep(1)
	code := m.Run()
	os.Exit(code)
}

func TestEchoAPI200Status(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "200 OK"
	response, _ := PostFormFile("test.csv", "file", input, "/echo")
	AssertEqual(t, response.Status, expected)
}

func TestEchoAPI200StatusResponseContent(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	response, _ := PostFormFile("test.csv", "file", input, "/echo")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), input)
}

func TestEchoAPI500Status(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "500 Internal Server Error"
	response, _ := PostFormFile("test.csv", "file", input, "/echo")
	AssertEqual(t, response.Status, expected)
}

func TestEchoAPI500StatusResponseContent(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "The matrix given is not a quadratic matrix. Found dimension 1x9. [[1 2 3 4 5 6 7 8 9]]\n"
	response, _ := PostFormFile("test.csv", "file", input, "/echo")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestInvertAPI200Status(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "200 OK"
	response, _ := PostFormFile("test.csv", "file", input, "/invert")
	AssertEqual(t, response.Status, expected)
}

func TestInvertAPI200StatusResponseContent(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "1,4,7\n2,5,8\n3,6,9"
	response, _ := PostFormFile("test.csv", "file", input, "/invert")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestInvertAPI500Status(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "500 Internal Server Error"
	response, _ := PostFormFile("test.csv", "file", input, "/invert")
	AssertEqual(t, response.Status, expected)
}

func TestInvertAPI500StatusResponseContent(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "The matrix given is not a quadratic matrix. Found dimension 1x9. [[1 2 3 4 5 6 7 8 9]]\n"
	response, _ := PostFormFile("test.csv", "file", input, "/invert")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestFlattenAPI200Status(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "200 OK"
	response, _ := PostFormFile("test.csv", "file", input, "/flatten")
	AssertEqual(t, response.Status, expected)
}

func TestFlattenAPI200StatusResponseContent(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "1,2,3,4,5,6,7,8,9"
	response, _ := PostFormFile("test.csv", "file", input, "/flatten")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestFlattenAPI500Status(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "500 Internal Server Error"
	response, _ := PostFormFile("test.csv", "file", input, "/flatten")
	AssertEqual(t, response.Status, expected)
}

func TestFlattenAPI500StatusResponseContent(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "The matrix given is not a quadratic matrix. Found dimension 1x9. [[1 2 3 4 5 6 7 8 9]]\n"
	response, _ := PostFormFile("test.csv", "file", input, "/flatten")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestSumAPI200Status(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "200 OK"
	response, _ := PostFormFile("test.csv", "file", input, "/sum")
	AssertEqual(t, response.Status, expected)
}

func TestSumAPI200StatusResponseContent(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "45"
	response, _ := PostFormFile("test.csv", "file", input, "/sum")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestSumAPI500Status(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "500 Internal Server Error"
	response, _ := PostFormFile("test.csv", "file", input, "/sum")
	AssertEqual(t, response.Status, expected)
}

func TestSumAPI500StatusResponseContent(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "The matrix given is not a quadratic matrix. Found dimension 1x9. [[1 2 3 4 5 6 7 8 9]]\n"
	response, _ := PostFormFile("test.csv", "file", input, "/sum")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestMultiplyAPI200Status(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "200 OK"
	response, _ := PostFormFile("test.csv", "file", input, "/multiply")
	AssertEqual(t, response.Status, expected)
}

func TestMultiplyAPI200StatusResponseContent(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "362880"
	response, _ := PostFormFile("test.csv", "file", input, "/multiply")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}

func TestMultiplyAPI500Status(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "500 Internal Server Error"
	response, _ := PostFormFile("test.csv", "file", input, "/multiply")
	AssertEqual(t, response.Status, expected)
}

func TestMultiplyAPI500StatusResponseContent(t *testing.T) {
	input := "1,2,3,4,5,6,7,8,9"
	expected := "The matrix given is not a quadratic matrix. Found dimension 1x9. [[1 2 3 4 5 6 7 8 9]]\n"
	response, _ := PostFormFile("test.csv", "file", input, "/multiply")
	body, _ := ioutil.ReadAll(response.Body)
	AssertEqual(t, string(body), expected)
}