package test

import (
	"fmt"
	"go-matrix/internal/controllers"
	"testing"
)

func TestEcho(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "1,2,3\n4,5,6\n7,8,9"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Echo)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestEchoWrongCSVFieldsSize(t *testing.T) {
	input := "A,B,C,1,2,3\n4,5,6\n7,8,9"
	expected := "Error when reading the file: 'record on line 2: wrong number of fields'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Echo)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestEchoWrongField(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "Error when retrieving the file sent under the keyword 'file': 'http: no such file'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "files", input, ctrl.Echo)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestEchoEmptyFile(t *testing.T) {
	input := ""
	expected := "The given file is empty\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Echo)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestEchoNonQuadraticMatrix(t *testing.T) {
	input := "1,2,3\n4,5,6"
	expected := "The matrix given is not a quadratic matrix. Found dimension 2x3. [[1 2 3] [4 5 6]]\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Echo)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestInvert(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "1,4,7\n2,5,8\n3,6,9"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Invert)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestInvertWrongCSVFieldsSize(t *testing.T) {
	input := "A,B,C,1,2,3\n4,5,6\n7,8,9"
	expected := "Error when reading the file: 'record on line 2: wrong number of fields'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Invert)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestInvertWrongField(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "Error when retrieving the file sent under the keyword 'file': 'http: no such file'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "files", input, ctrl.Invert)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestInvertEmptyFile(t *testing.T) {
	input := ""
	expected := "The given file is empty\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Invert)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestInvertNonQuadraticMatrix(t *testing.T) {
	input := "1,2,3\n4,5,6"
	expected := "The matrix given is not a quadratic matrix. Found dimension 2x3. [[1 2 3] [4 5 6]]\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Invert)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestFlatten(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "1,2,3,4,5,6,7,8,9"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Flatten)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestFlattenWrongCSVFieldsSize(t *testing.T) {
	input := "A,B,C,1,2,3\n4,5,6\n7,8,9"
	expected := "Error when reading the file: 'record on line 2: wrong number of fields'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Flatten)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestFlattenWrongField(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "Error when retrieving the file sent under the keyword 'file': 'http: no such file'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "files", input, ctrl.Flatten)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestFlattenEmptyFile(t *testing.T) {
	input := ""
	expected := "The given file is empty\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Flatten)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestFlattenNonQuadraticMatrix(t *testing.T) {
	input := "1,2,3\n4,5,6"
	expected := "The matrix given is not a quadratic matrix. Found dimension 2x3. [[1 2 3] [4 5 6]]\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Flatten)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSum(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "45"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSumNonInt(t *testing.T) {
	input := "1.1,2,3\n4,5,6\n7,8,9"
	expected := "Error when casting string to int: '1.1'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSumWrongCSVFieldsSize(t *testing.T) {
	input := "A,B,C,1,2,3\n4,5,6\n7,8,9"
	expected := "Error when reading the file: 'record on line 2: wrong number of fields'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSumWrongField(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "Error when retrieving the file sent under the keyword 'file': 'http: no such file'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "files", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSumEmptyFile(t *testing.T) {
	input := ""
	expected := "The given file is empty\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestSumNonQuadraticMatrix(t *testing.T) {
	input := "1,2,3\n4,5,6"
	expected := "The matrix given is not a quadratic matrix. Found dimension 2x3. [[1 2 3] [4 5 6]]\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Sum)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiply(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "362880"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiplyNonInt(t *testing.T) {
	input := "1.1,2,3\n4,5,6\n7,8,9"
	expected := "Error when casting string to int: '1.1'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiplyWrongCSVFieldsSize(t *testing.T) {
	input := "A,B,C,1,2,3\n4,5,6\n7,8,9"
	expected := "Error when reading the file: 'record on line 2: wrong number of fields'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiplyWrongField(t *testing.T) {
	input := "1,2,3\n4,5,6\n7,8,9"
	expected := "Error when retrieving the file sent under the keyword 'file': 'http: no such file'\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "files", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiplyEmptyFile(t *testing.T) {
	input := ""
	expected := "The given file is empty\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}

func TestMultiplyNonQuadraticMatrix(t *testing.T) {
	input := "1,2,3\n4,5,6"
	expected := "The matrix given is not a quadratic matrix. Found dimension 2x3. [[1 2 3] [4 5 6]]\n"
	ctrl := controllers.NewMatrixController()
	response := PostFormFileResponseRecorder("test.csv", "file", input, ctrl.Multiply)
	body := fmt.Sprint(response.Body)
	AssertEqual(t, body, expected)
}
