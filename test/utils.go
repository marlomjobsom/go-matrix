package test

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

const (
	PORT = "5000"
	URL = "http://localhost:" + PORT
)

// TimeSleep holds the execution for the given amount of seconds
func TimeSleep(sec int) {
	time.Sleep(time.Duration(sec * 1000000000))
}

// PostFormFile performs a real POST request that wraps a multipart/form-data that exercises the given end-point
func PostFormFile(fileName string, fieldName string, content string, endPoint string) (*http.Response, error) {
	request := BuildFormFileRequest(fileName, fieldName, content, endPoint)
	client := http.Client{}
	return client.Do(request)
}

// PostFormFileResponseRecorder performs a fake POST request that wraps a multipart/form-data that exercises the given function
func PostFormFileResponseRecorder(fileName string, fieldName string, content string, ctrlFunc func(w http.ResponseWriter, r *http.Request)) *httptest.ResponseRecorder {
	request := BuildFormFileRequest(fileName, fieldName, content, "")
	response := httptest.NewRecorder()
	handler := http.HandlerFunc(ctrlFunc)
	handler.ServeHTTP(response, request)
	return response
}

// BuildFormFileRequest builds a multipart/form-data request
func BuildFormFileRequest(fileName string, fieldName string, content string, endPoint string) *http.Request {
	buffer, writer := CreatFormFile(fileName, fieldName, content)
	request, _ := http.NewRequest(http.MethodPost, URL + endPoint, bytes.NewReader(buffer.Bytes()))
	request.Header.Set("Content-Type", writer.FormDataContentType())
	return request
}

// CreatFormFile builds a multipart/form-data
func CreatFormFile(fileName string, fieldName string, content string) (*bytes.Buffer, *multipart.Writer) {
	buffer := &bytes.Buffer{}
	writer := multipart.NewWriter(buffer)
	CreateFile(fileName, content)
	ReadFileToForm(fileName, fieldName, writer)
	DeleteFile(fileName)
	return buffer, writer
}

// CreateFile creates a local file
func CreateFile(fileName string, content string) {
	created, _ := os.Create(fileName)
	_, _ = created.Write([]byte(content))
	_ = created.Close()
}

// ReadFileToForm reads a local file and add its content to the given writer
func ReadFileToForm(fileName string, fieldName string, writer *multipart.Writer) {
	form, _ := writer.CreateFormFile(fieldName, fileName)
	opened, _ := os.Open(fileName)
	_, _ = io.Copy(form, opened)
	_ = writer.Close()
}

// DeleteFile deletes a file
func DeleteFile(fileName string) {
	_ = os.Remove(fileName)
}

// AssertEqual asserts whether two strings are equal
func AssertEqual(t *testing.T, got string, expected string) {
	if  got != expected {
		t.Errorf("Got: %s. Expected: %s", got, expected)
	}
}
